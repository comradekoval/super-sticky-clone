﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickyController : MonoBehaviour
{
    public GameController controller;
    public Animator animator;

    public bool IsDead { get; private set; }

    void Start()
    {
        IsDead = false;
    }

    void JumpEvent()
    {
        controller.JumpEvent();
    }

    public void ToxicDie()
    {
        animator.SetTrigger("Die");
        IsDead = true;
    }

    void DieEvent()
    {
        GetComponent<Renderer>().enabled = false;
    }
}
