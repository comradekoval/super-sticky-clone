﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointTextController : MonoBehaviour
{
    public FlyController fly;

    public float scalePerSecond;
    public float scaleSeconds;

    private Vector3 defaultScale;
    private float scaleTimeRemaining;

    void Awake()
    {
        gameObject.GetComponent<MeshRenderer>().sortingLayerName= "Default";    ///This is a hack; yet a working one
        gameObject.GetComponent<MeshRenderer>().sortingOrder = 10;
    }

    void Start()
    {
        scaleTimeRemaining = 0f;
        defaultScale = transform.localScale;
    }

    public void StartScaling()
    {
        Reset();
        scaleTimeRemaining = scaleSeconds;
    }

    private void Reset()
    {
        transform.localScale = defaultScale;
    }

    void Update()
    {
        if (scaleTimeRemaining >= 0f)
        {
            transform.localScale += new Vector3(scalePerSecond * Time.deltaTime, scalePerSecond * Time.deltaTime);
            scaleTimeRemaining -= Time.deltaTime;
            if (scaleTimeRemaining <= 0)
            {
                fly.gameObject.SetActive(false);
            }
        }
    }
}
