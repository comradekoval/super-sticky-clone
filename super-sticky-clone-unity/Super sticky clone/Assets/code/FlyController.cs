﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyController : MonoBehaviour
{
    public FlyPool pool;
    public PointTextController text;
    public SpriteRenderer spriteRenderer;

    public int points = 1;
    public float scaleSeconds = 1.5f;

    public bool isUsed { get; private set; }

    private Vector3 defaultScale;
    private float scaleTimeRemaining;
    private float scalePerSecond;

    void Start()
    {
        isUsed = false;
        scaleTimeRemaining = 0f;
        defaultScale = transform.localScale;
    }

    public void StartScaling()
    {
        scaleTimeRemaining = scaleSeconds;
        transform.localScale = defaultScale / 10f;
        scalePerSecond = (defaultScale.x - transform.localScale.x) / scaleSeconds;
    }

    public void Reset()
    {
        isUsed = false;
        spriteRenderer.enabled = true;
        transform.localScale = defaultScale;
        text.gameObject.SetActive(false);        
    }

    void Update()
    {
        if (scaleTimeRemaining >= 0f)
        {
            transform.localScale += new Vector3(scalePerSecond * Time.deltaTime, scalePerSecond * Time.deltaTime);
            scaleTimeRemaining -= Time.deltaTime;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (isUsed)
            return;
        isUsed = true;
        spriteRenderer.enabled = false;
        text.gameObject.SetActive(true);
        text.StartScaling();
        pool.RemoveFly();
    }
}
