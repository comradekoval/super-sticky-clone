﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToxicController : MonoBehaviour
{
    public StickyController controller;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        controller.ToxicDie();
    }
}
