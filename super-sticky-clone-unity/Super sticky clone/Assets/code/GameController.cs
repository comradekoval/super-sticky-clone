﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public SpriteRenderer stickyRenderer;
    public Rigidbody2D stickyRig;
    public Animator stickyAnimator;
    public StickyController sticky;

    public float jumpXForce = 5f;
    public float jumpYForce = 0.3f;
    bool isLatched = true;
    bool isLeft = true;
    bool jumpNeeded = false;

    void Update()
    {
        if (!sticky.IsDead)
        {
            if (Input.GetKeyDown(KeyCode.Mouse0) && isLatched && !jumpNeeded)
            {
                jumpNeeded = true;
            }
        }
    }

    void FixedUpdate()
    {
        if (jumpNeeded)
        {
            jumpNeeded = false;
            Jump();
        }
    }

    public void Jump()
    {
        SetLatch(false);
        float xForce = jumpXForce;
        if (!isLeft)
        {
            xForce = -xForce;
        }

        stickyRig.velocity = Vector2.zero;
        stickyRig.AddForce(new Vector2(xForce, jumpYForce));
        isLeft = !isLeft;
    }

    public void JumpEvent()
    {
        stickyRenderer.flipX = !stickyRenderer.flipX;
    }

    public void OnLatch()
    {
        SetLatch(true);
    }

    public void SetLatch(bool isLatched)
    {
        this.isLatched = isLatched;
        stickyAnimator.SetBool("Latched", isLatched);
    }
}
