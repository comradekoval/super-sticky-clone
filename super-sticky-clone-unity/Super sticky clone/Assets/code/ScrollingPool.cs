﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingPool : MonoBehaviour
{
    public GameObject wall;
    public SpriteRenderer wallRenderer;
    public StickyController stickyController;

    public int poolSize = 3;
    public float scrollSpeed = 0.01f;
    public float lowestHeight = -9f;

    private float groundHorizontalLength;
    private GameObject[] pool;

    private float wallSpriteHeight;

    void Awake()
    {
        wallSpriteHeight = wallRenderer.bounds.size.y;

        pool = new GameObject[poolSize];
        pool[0] = wall;

        for (int i = 1; i < 3; i++)
        {
            pool[i] = Instantiate(wall);
            pool[i].transform.position = wall.transform.position + new Vector3(0, i * wallSpriteHeight);
        }
    }

    void Update()
    {
        if (CheckCanScroll())
        {
            for (int i = 0; i < 3; i++)
            {
                pool[i].transform.position -= new Vector3(0, scrollSpeed);
                if (pool[i].transform.position.y <= lowestHeight)
                {
                    ReuseWall(pool[i]);
                }
            }
        }
    }

    private void ReuseWall(GameObject wall)
    {           
        wall.transform.position = wall.transform.position + new Vector3(0, poolSize * wallSpriteHeight);
    }

    private bool CheckCanScroll()
    {
        return !stickyController.IsDead && stickyController.transform.position.y > 0;
    }
}
