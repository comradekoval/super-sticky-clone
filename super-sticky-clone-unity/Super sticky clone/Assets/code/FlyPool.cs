﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyPool : MonoBehaviour
{
    public int maxFlies = 6;
    public float spawnMaxX = 1.6f;
    public float spawnMinY = -3.6f;
    public float spawnMaxY = 4.6f;
    public float maxPurpleFliesPercentage = 0.3f;
    public float minSpawnTime = 1f;
    public float maxSpawnTime = 2f;

    public GameObject fly;
    public GameObject purpleFly;

    private GameObject[] flies;
    private int curentFlies = 0;
    private float timeToSpawn;

    void Start()
    {
        flies = new GameObject[maxFlies + GetMaxFlies()];

        for (int i = 0; i < maxFlies; i++)
        {
            flies[i] = Instantiate(fly);
        }
        for (int i = maxFlies; i < maxFlies + GetMaxFlies(); i++)
        {
            flies[i] = Instantiate(purpleFly);
        }

        timeToSpawn = Random.Range(minSpawnTime, maxSpawnTime);
    }

    private int GetMaxFlies()
    {
        return (int)Mathf.Ceil(maxFlies * maxPurpleFliesPercentage);
    }

    void Update()
    {
        if (timeToSpawn >= 0)
        {
            timeToSpawn -= Time.deltaTime;
        }
        else
        {
            timeToSpawn = Random.Range(minSpawnTime, maxSpawnTime);
            SpawnFly();
        }
    }

    private void SpawnFly()
    {
        if (curentFlies >= maxFlies)
            return;
        GameObject currentFly = flies[Random.Range(0, flies.Length)];
        int fails = 0;                                                  ///With really bad luck and big no of flies used, code below can get really inefficient
        while (currentFly.activeSelf && fails < maxFlies)               // so we'll go somewhat pseudo-random
        {
            fails++;
            currentFly = flies[Random.Range(0, flies.Length)];
        }

        if (fails >= maxFlies)
        {
            for (int i = 0; i < flies.Length; i++)
            {
                if (!flies[i].activeSelf)
                {
                    currentFly = flies[i];
                    break;
                }
            }
        }

        currentFly.transform.position = new Vector3(Random.Range(-spawnMaxX, spawnMaxX), Random.Range(spawnMinY, spawnMaxY));
        currentFly.SetActive(true);
        currentFly.GetComponent<FlyController>().Reset();
        currentFly.GetComponent<FlyController>().StartScaling();
    }

    public void RemoveFly()
    {
        curentFlies--;
    }
}
